<?php
/**
 * @license_Application_file_docblock@
 */
/** Zend_Exception */
require_once ('Zend/Exception.php');
/**
 * Core_Exception
 *
 * Main Core Exception 
 *
 * @category   Core
 * @package    Core_Exception
 * @subpackage Exception
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_Exception extends Zend_Exception {

}

?>