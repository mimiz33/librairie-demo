<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Service_Model
 *
 * 
 *
 * @category   Core
 * @package    Core_Service
 * @subpackage Model
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_Service_Model {

	/**
	 * Factory for creating model object
	 *
	 * @param string $model table name
	 * @throws Core_Service_Exception
	 * @return Zend_Db_Table_Abstract
	 */
	public static function factory($model = null) {
		$ret = null;
		switch ($model) {
		case 'book':
			$ret = new Application_Model_DbTable_Book();
			break;
		case 'genre':
			$ret = new Application_Model_DbTable_Genre();
			break;
		case 'author':
			$ret = new Application_Model_DbTable_Author();
			break;
		default:
			throw new Core_Service_Exception('You must specify a Model');
		}
		return $ret;
	}

	
	/**
	 * This is used to save data in database from request
	 * 
	 * 
	 * 
	 * @param Zend_Controller_Request_Abstract $request
	 * @throws Zend_Db_Exception
	 * @throws Core_Exception
	 * @return int id of the inserted (or updated row)
	 */
	public static function saveFromRequestParams(Zend_Controller_Request_Abstract $request) {
		try {
			$model = self::factory($request->getParam('what'));
			$model->getAdapter()->getProfiler()->setEnabled(true);
			$id = null;
			
			if ($request->getParam('id') == -1) {
				$row = $model->createRow($request->getParams());
				$row->id = null;
			} else {
				$row = $model->fetchRow($model->select()->where('id = ?', $request->getParam('id')));
				$row->setFromArray($request->getParams());
			}
			if ($model instanceof Application_Model_DbTable_Book) {
				$id = $row->save();
				// Need to save link to authors
				$authorBook = new Application_Model_DbTable_Authorbook();
				$newAb = $authorBook->createRow(array('id'=>null, 'author_id'=>$request->getParam('author'), 'book_id'=>$id));
				$newAb->save();
			} else {
				
				$id = $row->save();
			}
			return $id;
		} catch (Zend_Db_Exception $zde) {
			$query  = $model->getAdapter()->getProfiler()->getLastQueryProfile()->getQuery();
			$params = $model->getAdapter()->getProfiler()->getLastQueryProfile()->getQueryParams();
			$model->getAdapter()->getProfiler()->setEnabled(false);
			throw new Zend_Db_Exception($zde->getMessage() . '<br />'. $query. '<br />'. print_r($params,true));
		}
		catch (Core_Exception $ce) {
			throw $ce;
		}
	}
}
