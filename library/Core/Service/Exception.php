<?php
/**
 * @license_Application_file_docblock@
 */
/** Core_Exception */
require_once ('Core/Exception.php');
/**
 * Core_Service_Exception
 *
 * Main Service Exception
 *
 * @category   Core
 * @package    Core_Exception
 * @subpackage Service
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_Service_Exception extends Core_Exception {

}

?>