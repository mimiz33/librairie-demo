<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Module_Controller_Action
 *
 * Action Controller defaults for modules (just in case of)
 *
 * @category   Core
 * @package    Core_Module
 * @subpackage Controller
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_Module_Controller_Action extends Core_Controller_Action {
	
}