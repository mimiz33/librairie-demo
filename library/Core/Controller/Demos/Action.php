<?php
/**
@license_Application_file_docblock@
 */
/**
 * Core_Controller_Action
 *
 * Action Controller defaults(just in case of)
 *
 * @category   Core
 * @package    Core_Controller
 * @subpackage Action
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_Controller_Demos_Action extends Core_Controller_Action {

	/**
	 * Class initialisation
	 * @return void
	 */
	public function init() {

		$page = $this->view->navigation()->findOneByLabel('demo.'.strtolower($this->getRequest()->getControllerName())); /* @var $page Zend_Navigation_Page */
		if ($page) {
			$page->setActive();
		}
		$this->_init();
	}

	/**
	 * Show Home Page
	 */
	public function indexAction() {
		$this->_forward("list", $this->getRequest()->getControllerName(), $this->getRequest()->getModuleName(), array('what' => 'book'));
	}

	/**
	 * Action used for updating datas
	 * @throws Core_Exception
	 */
	public function editAction() {
		if ($this->getRequest()->getParam('what') == null) {
			throw new Core_Exception('You must specify what you want to edit (author, genre, book)');
		}
		$form = Core_Form::factory($this->getRequest()->getParam('what'));
		$form->setAttrib('id', 'form-edit-'.$this->getRequest()->getParam('what'));
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getParams())) {
				try {
					$id = Core_Service_Model::saveFromRequestParams($this->getRequest());
					$this->_redirect($this->view->url(array(
						'module' => 'default',
						'controller' => $this->getRequest()->getControllerName(),
						'action' => 'list',
						'what' => $this->getRequest()->getParam('what')
					)));
				} catch (Core_Exception $ce) {
					throw $ce;
				}
			}
		} else {
			$model = Core_Service_Model::factory($this->getRequest()->getParam('what'));
			$row = $model->fetchRow($model->select()->where('id = ? ', $this->getRequest()->getParam('id')));
			$form->populate($row->toArray());
		}
		$this->view->form = $form;
	}

	/**
	 * Action used for adding datas
	 * @throws Core_Exception
	 */
	public function addAction() {
		if ($this->getRequest()->getParam('what') == null) {
			throw new Core_Exception('You must specify what you want to edit (author, genre, book)');
		}
		$form = Core_Form::factory($this->getRequest()->getParam('what'));
		$form->setAttrib('id', 'form-add-'.$this->getRequest()->getParam('what'));
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getParams())) {
				try {
					$id = Core_Service_Model::saveFromRequestParams($this->getRequest());
					$this->_redirect($this->view->url(array(
						'module' => 'default',
						'controller' => $this->getRequest()->getControllerName(),
						'action' => 'list',
						'what' => $this->getRequest()->getParam('what')
					)));
				} catch (Core_Exception $ce) {
					throw $ce;
				} 
				
			}
		}
		$this->view->form = $form;
	}

	/**
	 * Action used for deleting datas
	 * @throws Core_Exception
	 */
	public function deleteAction() {
		if ($this->getRequest()->getParam('what') == null) {
			throw new Core_Exception('You must specify what you want to edit (author, genre, book)');
		}
		$form = new Application_Model_Forms_Delete();
		$form->setAttrib('id', 'form-delete-'.$this->getRequest()->getParam('what'));
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getParams())) {
				try {
					$model = Core_Service_Model::factory($this->getRequest()->getParam('what'));
					$row = $model->fetchRow($model->select()->where('id = ?', $this->getRequest()->getParam('id')));
					$row->delete();
					$model->getAdapter()->getProfiler()->setEnabled(false);
					$this->_redirect($this->view->url(array(
						'module' => 'default',
						'controller' => $this->getRequest()->getControllerName(),
						'action' => 'list',
						'what' => $this->getRequest()->getParam('what')
					)));
				} catch (Core_Exception $ce) {
					throw $ce;
				}
			}
		} else {
			$datas = array(
				'id' => $this->getRequest()->getParam('id'),
				'what' => $this->getRequest()->getParam('what')
			);
		}
		$this->view->form = $form;
	}

	/**
	 * List some table Elements
	 */
	public function listAction() {
		if ($this->getRequest()->getParam('what') == null) {
			throw new Core_Exception('You must specify what you want to list (author, genre, book)');
		}
		if ($this->getRequest()->getParam('what') == 'book') {
			//$this->_forward('index', $this->getRequest()->getControllerName(), 'demos');
			$bModel = new Application_Model_DbTable_Book();
			$books = $bModel->getAllBooks();

			$this->view->books = $books;
			$this->renderScript(strtolower($this->getRequest()->getControllerName()).'/index.phtml');
		} else {
			$model = Core_Service_Model::factory($this->getRequest()->getParam('what'));
			$datas = $model->fetchAll();
			$this->view->what = $this->getRequest()->getParam('what');
			$this->view->datas = $datas->toArray();
		}
	}

	/**
	 * Initialize object
	 *
	 * Called from {@link __construct()} as final step of object instantiation.
	 *
	 * @return void
	 */
	public function _init() {

	}
}
