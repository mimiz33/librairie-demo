<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Controller_Exception
 *
 * For Exceptions in controller
 *
 * @category   Core
 * @package    Core_Controller
 * @subpackage Exception
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
 class Core_Controller_Exception extends Core_Exception {
 	
 }