<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Form
 * 
 * A simple override for customization purpose
 *
 * @category   Core
 * @package    Core_Form
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
class Core_Form extends Zend_Form {

	public function __construct($options=null) {
		$this->addElementPrefixPath('Core_Form_Decorator',
				'Core/Form/Decorator',
				'DECORATOR');
		parent::__construct($options);
	}
	
	/**
	 * 
	 * @param unknown_type $form_type
	 * @throws Core_Form_Exception
	 * @return Core_Form
	 */
	public static function factory($form_type = null) {
		$form = null;
		switch ($form_type) {
			case 'book':
				$form = new Application_Model_Forms_Book();
				break;
			case 'author':
				$form = new Application_Model_Forms_Author();
				break;
			case 'genre':
				$form = new Application_Model_Forms_Genre();
				break;
			default:
				throw new Core_Form_Exception('You must specify a form type');
		}
		return $form;
	}

}
?>