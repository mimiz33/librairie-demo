 <?php
 /**
  * @license_Application_file_docblock@
 */
 /**
  * Core_Tools_File
 *
 * Class for mapping infos.json of demos
 *
 * @category   Core
 * @package    Core_Tools
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
 class Core_Tools_File {
 
 	private $_datas;
 	
 	public function __construct() {
 		$this->_datas = array();
 	}
 
 	public function __get($property) {
 		
 		return $this->_datas[$property];
 	}
 
 	public function __set($property, $value) {
 		$this->_datas[$property] = $value;
 	}
 
 	/**
 	 * Load Tools information from file definition (infos.json)
 	 *
 	 * @param string $file File path (absolute, or relative from DEMOS_DIRECTORY
 	 * @return Core_Tools_File
 	 */
 	public static function loadFromFile($file = null) {
 		if ($file === null) {
 			throw new Core_Tools_Exception("You must specify a filename");
 		}
 		$fileFullPath = "";
 		if (is_file($file)) {
 			$fileFullPath = $file;
 		} else if (is_file(APPLICATION_PATH.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."public".DIRECTORY_SEPARATOR.'implementations'.DIRECTORY_SEPARATOR.$file)) {
 			$fileFullPath = APPLICATION_PATH.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."public".DIRECTORY_SEPARATOR.'implementations'.DIRECTORY_SEPARATOR.$file;
 		} else {
 			throw new Core_Tools_Exception("Unable to find $file");
 		}
 		$data = file_get_contents($fileFullPath);
 		return self::loadFromJson($data);
 	}
 
 
 	/**
 	 * Load Tools information from json
 	 *
 	 * @param string $json JSON String
 	 * @return Core_Tools_File
 	 */
 	public static function loadFromJson($json= null) {
 		if ($json === null) {
 			throw new Core_Tools_Exception("You must specify a JSON String");
 		}
 		try {
 			$datas = Zend_Json_Decoder::decode($json);
 			$tools = new self();
 			foreach( $datas as $k => $v ) {
 				$tools->$k = $v;
 			}
 			return $tools;
 		} catch ( Zend_Json_Exception $zje ) {
 			throw new Core_Tools_Exception("Unable to decode JSON String with message : \n".$zje->getMessage());
 		}
 	}
 }
 