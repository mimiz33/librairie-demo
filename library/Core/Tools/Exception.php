<?php
/**
 * @license_Application_file_docblock@
 */
/** Core_Exception */
require_once ('Core/Exception.php');
/**
 * Core_Tools_Exception
 *
 * Core Tools Exception
 *
 * @category   Core
 * @package    Core_Tools
 * @subpackage Exception
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_Tools_Exception extends Core_Exception {
	
}