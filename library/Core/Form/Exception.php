<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Form_Exception
 *
 * Forms Exception
 *
 * @category   Core
 * @package    Core_Form
 * @subpackage Exception
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
 class Core_Form_Exception extends Core_Exception {
 	
 }