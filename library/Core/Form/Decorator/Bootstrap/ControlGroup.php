<?php
/**
 * ${license_Core_file_docblock}
 */

/** Zend_Form_Decorator_Abstract */
require_once 'Zend/Form/Decorator/Abstract.php';

/**
 * Core_Form_Decorator_Bootstrap_ControlGroup
 *
 * Creates a div element with control group class for twitter boostrap.
 *
 * @category   Core
 * @package    Core_Form
 * @subpackage Decorator
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
class Core_Form_Decorator_Bootstrap_ControlGroup extends Zend_Form_Decorator_Abstract
{
    /**
     * Default placement: surround content
     * @var string
     */
    protected $_placement = null;

    /**
     * Render
     *
     * Renders as the following:
     * <dt>$dtLabel</dt>
     * <dd>$content</dd>
     *
     * $dtLabel can be set via 'dtLabel' option, defaults to '\&#160;'
     *
     * @param  string $content
     * @return string
     */
    public function render($content)
    {
        $class = '';
    	if( $this->getElement()->hasErrors() ) {
    		$class = 'error'; 
    	} 

        return '<div class="control-group '.$class.'">'.$content.'</div>';
    }
}
