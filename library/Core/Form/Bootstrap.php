<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Form_Bootstrap
 *
 * A simple extension for creating forms in twitter Bootstrap 
 *
 * @category   Core
 * @package    Core_Form
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
class Core_Form_Bootstrap extends Core_Form {
	
	
	public function __construct($options = null) {
		$this->addElementPrefixPath('Core_Form_Decorator_Bootstrap',
				'Core/Form/Decorator/Bootstrap',
				'DECORATOR');
	
		
		parent::__construct($options);
	}
	
	/**
	 * Load the default decorators
	 * @see Zend_Form
	 * @return Core_Form
	 */
	public function loadDefaultDecorators() {
		if ($this->loadDefaultDecoratorsIsDisabled()) {
			return $this;
		}
	
		$decorators = $this->getDecorators();
		if (empty($decorators)) {
			$this->addDecorator('FormElements')->addDecorator('Form');
		}
		return $this;
	}
	
	/**
	 * This is the main implementation for the Bootstap environment
	 * Define decorators or simply customize defaults decorators
	 */
	private function _loadBootstrapDecorators() {
		/*
		 default decorator
			'ViewHelper',
			'Errors',
			array('Description', array('tag' => 'p', 'class' => 'description')),
			array('HtmlTag', array('tag' => 'dd')),
			array('Label', array('tag' => 'dt'))
		*/
		
		// need to change form elements
		foreach ($this->getElements() as $element) {
			switch (get_class($element)) {
				case "Zend_Form_Element_Hash":
				case "Zend_Form_Element_Hidden":
					$element->setDecorators(array(
					'ViewHelper',
					'Errors',
					array(array('data' => 'HtmlTag'), array('tag' => 'div'))
					));
		
					break;
				case "Zend_Form_Element_Textarea":
				case "Zend_Form_Element_Text":
					$element->setDecorators(array(
						'ViewHelper',
						'Errors',
						array('Description', array('tag' => 'p', 'class' => 'help-block')),
						array('HtmlTag', array('tag' => 'div', 'class'=>'controls')),
						array('Label'),
						array('ControlGroup')
					));
		
					break;
				case "Zend_Form_Element_Submit":
					$element->setAttrib('class', 'btn btn-primary');
					$element->setDecorators(array(
							'ViewHelper'
					));
					break;
				default:
		
			}
		
		}
	}
	
	/**
	 * Render form
	 *
	 * @see Zend_Form::render
	 * @param  Zend_View_Interface $view
	 * @return string
	 */
	public function render(Zend_View_Interface $view = null) {
		$this->_loadBootstrapDecorators();
		return parent::render($view);
	}
}