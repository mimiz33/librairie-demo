<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_Tools
 *
 * 
 *
 * @category   Core
 * @package    Core_Tools
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
 class Core_Tools {
 	
 	
 	
 	public static function getAllImplementations() {
 		$imp = Zend_Registry::get('IMPLEMENTATION');
 		$files = glob( $imp["directory"] . "/*/infos.json" );
 		$tools = array();
 		foreach ( $files as $file ) {
 			array_push($tools, Core_Tools_File::loadFromFile($file));
 		}
 		return $tools;
 	}
 }