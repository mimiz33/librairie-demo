<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Core_View_Helper_SelectedAction
 *
 * View Helper for selected action
 *
 * @category   Core
 * @package    Core_View
 * @subpackage Helpers
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_View_Helper_SelectedAction extends Zend_View_Helper_Abstract {
	
	public function selectedAction($action_name) {
		$zfc = Zend_Controller_Front::getInstance();
		$request = $zfc->getRequest();
		$params = explode('-', $action_name);
		if( $params[0] == $request->getActionName() && $params[1] == $request->getParam('what') ) {
			return 'warning selected';
		} 
		return 'primary';
	}
}