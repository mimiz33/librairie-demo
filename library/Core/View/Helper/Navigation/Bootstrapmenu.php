<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * @see Zend_View_Helper_Navigation_HelperAbstract
 */
require_once 'Zend/View/Helper/Navigation/HelperAbstract.php';

/**
 * Helper for rendering menus from Bootstrap navigation containers
 *
 * @category   Core
 * @package    Core_View
 * @subpackage Helper
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version	   @@version@@
 */
class Core_View_Helper_Navigation_Bootstrapmenu extends Zend_View_Helper_Navigation_Menu
{

	/**
	 * CSS class to use for the ul element
	 *
	 * @var string
	 */
	protected $_ulClass = 'nav';

	/**
	 * View helper entry point:
	 * Retrieves helper and optionally sets container to operate on
	 *
	 * @param  Zend_Navigation_Container $container  [optional] container to
	 *                                               operate on
	 * @return Zend_View_Helper_Navigation_Menu      fluent interface,
	 *                                               returns self
	 */
	public function bootstrapmenu(Zend_Navigation_Container $container = null)
	{
		return parent::menu($container);
	}

	/**
	 * Intercept renderMenu() call and apply custom Twitter Bootstrap class/id
	 * attributes.
	 *
	 * @see Zend_View_Helper_Navigation_Menu::renderMenu()
	 * @param Zend_Navigation_Container $container (Optional) The navigation container.
	 * @param array $options (Optional) Options for controlling rendering.
	 *
	 * @return string
	 */
	public function renderMenu(Zend_Navigation_Container $container = null, array $options = array())
	{
		return $this->applyBootstrapClassesAndIds(parent::renderMenu($container, $options));
	}


	/**
	 * Applies the custom Twitter Bootstrap dropdown class/id attributes where
	 * necessary.
	 *
	 * @param string $html The HTML
	 * @return string
	 */
	protected function applyBootstrapClassesAndIds($html)
	{
		$domDoc = new DOMDocument('1.0', 'utf-8');
		$domDoc->loadXML('<?xml version="1.0" encoding="utf-8"?>' . $html);

		$xpath = new DOMXPath($domDoc);

		foreach ($xpath->query('//a[starts-with(@href, "#")]') as $item)
		{
			$result = $xpath->query('../ul', $item);

			if ($result->length === 1)
			{
				$ul = $result->item(0);
				$ul->setAttribute('class', 'dropdown-menu');

				$item->parentNode->setAttribute('id', substr($item->getAttribute('href'), 1));
				$item->parentNode->setAttribute('class', $item->parentNode->getAttribute('class').' dropdown');
				
				$item->setAttribute('data-toggle', 'dropdown');

				if (($existingClass = $item->getAttribute('class')) !== '')
				{
					$item->setAttribute('class', $item->getAttribute('class') . ' dropdown-toggle');
				}
				else
				{
					$item->setAttribute('class', 'dropdown-toggle');
				}

				$caret = $domDoc->createElement('b', '');
				$caret->setAttribute('class', 'caret');

				$item->appendChild($caret);
			}
		}

		return $domDoc->saveXML($xpath->query('/ul')->item(0));
	}


}
