<?php
/**
 * @license_Application_file_docblock@
 */
/** Zend_View_Helper_Translate */
require_once ('Zend/View/Helper/Translate.php');
/**
 * Zend_View_Helper_Translate
 *
 * This class is an override of the main tranlation view helper 
 * It allows to use file as translation value, il messageID (language key) 
 * starts with file:// it will scan for a file 
 *
 * @category   Core
 * @package    Core_View
 * @subpackage Helper
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Core_View_Helper_Translate extends Zend_View_Helper_Translate {
	public function translate($messageID = null) {
		$ret = parent::translate($messageID);
		if ( substr($ret,0,7) == 'file://' ) {
			$filename = substr($ret,7);
			$file = APPLICATION_PATH . DIRECTORY_SEPARATOR . "languages/files" . DIRECTORY_SEPARATOR . $filename;
			if ( is_file($file) ) {
				return file_get_contents($file);
			} else {
				$this->getTranslator()->_log($filename . "does not exists");
			}
		}
		return $ret;
	}
}