<?php
return array(
	'site.name' => "DEMO LIBRAIRIE",
	'Documentation' => 'Documentation',
	'fr.intro.documentation.html' => 'file://fr.intro.documentation.html',
	'Tools' => 'Outils',
	'Home' => 'Accueil',
	'demo.standard' => "Standard Sans JS",
	'library' => 'Librairie',
	'library.add.book' => 'Ajouter un livre',
	'library.add.author' => 'Ajouter un auteur',
	'library.add.genre' => 'Ajouter un genre',
	'library.list.book' => 'Liste des livres',
	'library.list.author' => 'Liste des auteurs',
	'library.list.genre' => 'Liste des genres',
	
	'library.empty' => 'La librairie est vide !',
	'site.description.home' => 'file://description.home.fr.html',
	// forms
	'forms.author.label.name' => 'nom',
	'forms.author.description.name' => "Saisissez le nom de l'auteur.",
	'forms.author.label.surname' => 'prénom',
	'forms.author.label.submit' => "Envoyer",
	'forms.book.label.title' => 'Titre du livre',
	'forms.book.label.author' => 'Auteur du livre',
	'forms.book.label.description' => 'Desription du livre',
	'forms.book.label.genre' => 'Genre du livre',
	'forms.genre.label.label' => 'Libellé du genre',
	// Forms validate
	'isEmpty' => "La valeur est requise et ne peut être vide",
	'notEmptyInvalid' => 'Type invalide ...',
	'notSame' => 'Les clés de sécurité ne correspondent pas.',
	'missingToken' => 'Aucune clé de sécurité fournie.'
);