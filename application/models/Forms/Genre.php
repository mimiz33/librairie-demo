<?php 
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_Forms_Genre
 * 
 * Form object for adding Genres
 * 
 * @category   Application
 * @package    Application_Model
 * @subpackage Forms
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_Forms_Genre extends Core_Form_Bootstrap
{

    public function init(){
    	
    	$only_alpha = new Zend_Validate_Alnum();
    	$hash = new Zend_Form_Element_Hash('csrf');
    	
    	$id = new Zend_Form_Element_Hidden('id');
    	$id->setValue(-1);
    	
    	$label = new Zend_Form_Element_Text("label");
    	$label->setLabel("forms.genre.label.label");
    	$label->setRequired(true);
    	$label->setAllowEmpty(false);
    	$label->addValidators(array($only_alpha));
    	
    	
    	
    	$submit = new Zend_Form_Element_Submit("submit");
    	$submit->setLabel("forms.author.label.submit");
    	
    	$this->addElements(array($hash,$id,$label, $submit));
    }


}