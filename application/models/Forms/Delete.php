<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_Forms_Delete
 * 
 * Form object for deleting Confirmation in database
 * 
 * @category   Application
 * @package    Application_Model
 * @subpackage Forms
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_Forms_Delete extends Core_Form_Bootstrap
{

	/**
	 * Form Initialisation
	 * @see Zend_Form::init()
	 */
    public function init(){
    	
    	// This is for CSRF protection
    	$hash = new Zend_Form_Element_Hash('csrf');
    	
    	$id = new Zend_Form_Element_Hidden('id');
    	$id->setValue(-1);
    	
    	$what = new Zend_Form_Element_Hidden('what');
    	$what->setValue(null);
    	
    	$submit = new Zend_Form_Element_Submit("submit");
    	$submit->setLabel("forms.author.label.submit");
    	
    	$this->addElements(array($hash,$id,$what, $submit));
    	
    }


}