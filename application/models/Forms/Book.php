<?php 
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_Forms_Book
 *
 * Form object for adding Bookss
 *
 * @category   Application
 * @package    Application_Model
 * @subpackage Forms
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_Forms_Book extends Core_Form_Bootstrap
{

    public function init(){
    	
    	$only_alpha = new Zend_Validate_Alnum();
    	$hash = new Zend_Form_Element_Hash('csrf');
    	
    	$id = new Zend_Form_Element_Hidden('id');
    	$id->setValue(-1);
    	
    	$title = new Zend_Form_Element_Text("title");
    	$title->setLabel("forms.book.label.title");
    	$title->setRequired(true);
    	$title->setAllowEmpty(false);
    	//$title->addValidators(array($only_alpha));
    	
    	$author = new Zend_Form_Element_Select("author");
    	$author->setLabel("forms.book.label.author");
    	$author->setRequired(true);
    	
    	$options = new Application_Model_DbTable_Author();
    	$select = $options->select()->order("name, surname");
    	$res = $options->fetchAll($select);
    	foreach ($res as $aut) {
    		$author->addMultiOption($aut->id,$aut->name." ".$aut->surname);
    	}
    	
    	
    	$description= new Zend_Form_Element_Textarea("description");
    	$description->setLabel("forms.book.label.description");
    	$description->setRequired(true);
    	$description->setAllowEmpty(false);
    	$description->setAttribs(array('rows'=> 5, 'cols'=>20));
    	//$description->addValidators(array($only_alpha));
    	
    	$genre = new Zend_Form_Element_Select("genre_id");
    	$genre->setLabel("forms.book.label.genre");
    	$genre->setRequired(true);
    	 
    	$options = new Application_Model_DbTable_Genre();
    	$select = $options->select()->order("label");
    	$res = $options->fetchAll($select);
    	foreach ($res as $gen) {
    		$genre->addMultiOption($gen->id,$gen->label);
    	}
    	
    	
    	$submit = new Zend_Form_Element_Submit("submit");
    	$submit->setLabel("forms.author.label.submit");
    	
    	$this->addElements(array($hash,$id,$title, $author, $description, $genre, $submit));
    }


}