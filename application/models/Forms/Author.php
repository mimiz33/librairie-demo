<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_Forms_Author
 * 
 * Form object for adding Authors
 * 
 * @category   Application
 * @package    Application_Model
 * @subpackage Forms
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_Forms_Author extends Core_Form_Bootstrap
{

	/**
	 * Form Initialisation
	 * @see Zend_Form::init()
	 */
    public function init(){
    	
    	
    	    	
    	$only_alpha = new Zend_Validate_Alnum();
    	
    	// This is for CSRF protection
    	$hash = new Zend_Form_Element_Hash('csrf');
    	
    	$id = new Zend_Form_Element_Hidden('id');
    	$id->setValue(-1);
    	
    	$name = new Zend_Form_Element_Text("name");
    	$name->setLabel("forms.author.label.name");
    	$name->setRequired(true);
    	$name->setAllowEmpty(false);
    	$name->addValidators(array($only_alpha));
    	$name->setDescription("forms.author.description.name");
    	
    	$surname = new Zend_Form_Element_Text("surname");
    	$surname->setLabel("forms.author.label.surname");
    	$surname->setRequired(true);
    	$surname->setAllowEmpty(false);
    	$surname->addValidators(array($only_alpha));
    	
    	$submit = new Zend_Form_Element_Submit("submit");
    
    	$submit->setLabel("forms.author.label.submit");
    	
    	$this->addElements(array($hash,$id,$name, $surname, $submit));
    	
    }


}