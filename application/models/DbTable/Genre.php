<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_DbTable_Genre
 *
 * ORM Table Genre Mapping
 *
 * @category   Application
 * @package    Application_Model
 * @subpackage DbTable
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_DbTable_Genre extends Zend_Db_Table_Abstract
{

    protected $_name = 'genre';
    protected $_primary = "id";
    


}