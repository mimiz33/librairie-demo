<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_DbTable_Book
 *
 * ORM Table Book Mapping
 *
 * @category   Application
 * @package    Application_Model
 * @subpackage DbTable
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_DbTable_Book extends Zend_Db_Table_Abstract
{

    
    protected $_name = 'book';
    protected $_primary = "id";

    protected $_dependentTables = array('Application_Model_DbTable_Authorbook');
    
    protected $_referenceMap = array(
    		'Application_Model_DbTable_Genre' => array(
    				'columns' => 'genre_id',
    				'refTableClass' => 'Application_Model_DbTable_Genre',
    				'refColumns' => 'id'
    		),
    );
    
    /**
     * Return all books with all dependents tables
     * 
     * @param string $format
     * @return array 
      */
    public function getAllBooks() {
    	$tab_return_all = array();
    	$books = $this->fetchAll();
    	//$tab_return = $books->toArray();
    	foreach($books as $book) {
    		$tab_return = $book->toArray();
    		$authors = $book->findManytoManyRowset('Application_Model_DbTable_Author', 'Application_Model_DbTable_Authorbook');
    		$tab_return['authors'] = $authors->toArray();
    		$genre  = $book->findParentRow('Application_Model_DbTable_Genre');
    		$tab_return['genre'] = $genre->toArray();
	    	array_push($tab_return_all, $tab_return);
    	}
    	return $tab_return_all;
    }

}