<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_DbTable_Author
 *
 * ORM Table Author Mapping
 *
 * @category   Application
 * @package    Application_Model
 * @subpackage DbTable
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_DbTable_Author extends Zend_Db_Table_Abstract {

	protected $_name = 'author';
	protected $_primary = "id";

	protected $_dependentTables = array('Application_Model_DbTable_Authorbook');

}
