<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Application_Model_DbTable_Authorbook
 *
 * ORM Table Authorbook Mapping
 *
 * @category   Application
 * @package    Application_Model
 * @subpackage DbTable
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Application_Model_DbTable_Authorbook extends Zend_Db_Table_Abstract
{

    protected $_name = 'author_book';
    protected $_primary = "id";
    
    protected $_referenceMap = array(
    		'Application_Model_DbTable_Author' => array(
    				'columns' => 'author_id',
    				'refTableClass' => 'Application_Model_DbTable_Author',
    				'refColumns' => 'id',
    				'onDelete' => self::CASCADE
    				
    		),
    		'Application_Model_DbTable_Book' => array(
    				'columns' => 'book_id',
    				'refTableClass' => 'Application_Model_DbTable_Book',
    				'refColumns' => 'id',
    				'onDelete'          => self::CASCADE
    		)
    );


}