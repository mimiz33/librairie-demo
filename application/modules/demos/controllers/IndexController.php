<?php 
/**
 * @license_Application_file_docblock@
 */
/**
 * Demos_IndexController
 *
 * Index Controller for Demos Module
 *
 * @category   Application
 * @package    Demos
 * @subpackage Controller
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Demos_IndexController extends Core_Controller_Action
{
	public function _init() {
		
	}
	
	/**
	 * Show a list of Implementations
	 */
	public function indexAction() {
		
		$this->view->tools = Core_Tools::getAllImplementations(); 
	}
	
	public function demosAction() {
		$this->_changeLayout();
	}
	
	private function _changeLayout() {
		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('demos');
		// reseting layout
		$layout->getView()->clearVars();
		$layout->getView()->headTitle("Demo for ", Zend_View_Helper_Placeholder_Container_Abstract::SET);
		$layout->getView()->headLink()->setContainer(new Zend_View_Helper_Placeholder_Container());
		$layout->getView()->headScript()->setContainer(new Zend_View_Helper_Placeholder_Container());
		$layout->getView()->headLink()->appendStylesheet('/style/bootstrap/css/bootstrap.min.css');
		$layout->getView()->headLink()->appendStylesheet('/style/demos.css');
		$layout->getView()->headScript()->appendFile('/tools/jquery-1.7.2.min.js');
		$layout->getView()->headScript()->appendFile('/style/bootstrap/js/bootstrap.min.js');
		
		// il faut un default theme !
		// On ne charge que le js du gars !
		
		
	}
	
} 