<?php
/**
 * @license_Application_file_docblock@
 */
/**
 * Api_RestController
 *
 * Rest Controller for retreving datas
 *
 * @category   Application
 * @package    Api
 * @subpackage Controller
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class Api_RestController extends Zend_Rest_Controller {
	
	/**
	 *
	 * @var Core_Service_Livres
	 */
	private $livresService = null;
	
	public function init() {
		/* Initialize action controller here */
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout->disableLayout ();
		$this->livresService = new Core_Service_Livres ();
	}
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		// TODO Auto-generated IndexController::indexAction() default action
		$this->getResponse ()->setBody ( $this->livresService->getLivres () );
		$this->getResponse ()->setHttpResponseCode ( 200 );
	}
	public function getAction() {
		$this->getResponse ()->setBody ( sprintf ( 'Resource #%s', $this->_getParam ( 'id' ) ) );
		$this->getResponse ()->setHttpResponseCode ( 200 );
	}
	
	/**
	 * Used When Data is posted !
	 * 
	 * @see Zend_Rest_Controller::postAction()
	 */
	public function postAction() {
		//$this->log(print_r($this->getRequest()->getRawBody(), true));
		$rBody = $this->getRequest()->getRawBody();
		
		$params = Zend_Json::decode($rBody, Zend_Json::TYPE_ARRAY);
		$array = array (
				'id' => isset($params['id'])?$params ['id']:null,
				'title' => $params ['title'],
				'author' => $params ['author'],
				'image' => $params ['image'],
				'synopsis' => isset($params['synopsis'])?$params ['synopsis']:'',
				'category' => $params ['category'],
				'isbn' => $params ['ISBN'] 
		);
		
		try {
			if ($this->livresService->addBook ( $array )) {
				$this->getResponse ()->setBody('Book Added to librairie');
				$this->getResponse ()->setHttpResponseCode ( 201 );
			} else{
				throw new Core_Exception('Unable to add Book');
			}
		} catch ( Core_Exception $ce ) {
			$this->getResponse ()->setBody('Core Exception Error !\n' . $ce->getMessage());
			$this->getResponse ()->setHttpResponseCode ( 500 );
		} catch ( Zend_Exception $ze ) {
			$this->getResponse ()->setBody('Zend Exception Error !\n'.$ze->getMessage());
			$this->getResponse ()->setHttpResponseCode ( 500 );
		}
	
	}
	
	public function putAction() {
		$rBody = $this->getRequest()->getRawBody();
		
		$params = Zend_Json::decode($rBody, Zend_Json::TYPE_ARRAY);
		$array = array (
				'id' => isset($params['id'])?$params ['id']:null,
				'title' => $params ['title'],
				'author' => $params ['author'],
				'image' => $params ['image'],
				'synopsis' => isset($params['synopsis'])?$params ['synopsis']:'',
				'category' => $params ['category'],
				'isbn' => $params ['ISBN'] 
		);
		
		try {
			if ($this->livresService->editBook ( $array )) {
				$this->getResponse ()->setBody("Book $id Updated");
				$this->getResponse ()->setHttpResponseCode ( 201 );
			} else{
				throw new Core_Exception('Unable to update Book !');
			}
		} catch ( Core_Exception $ce ) {
			$this->getResponse ()->setBody('Core Exception Error !\n' . $ce->getMessage());
			$this->getResponse ()->setHttpResponseCode ( 500 );
		} catch ( Zend_Exception $ze ) {
			$this->getResponse ()->setBody('Zend Exception Error !\n'.$ze->getMessage());
			$this->getResponse ()->setHttpResponseCode ( 500 );
		}
	}
	
	public function deleteAction() {
		try {
			$id = $this->_getParam ( 'id' );
			if ($this->livresService->deleteBook( $id )) {
				$this->getResponse ()->setBody("Book $id deleted");
				$this->getResponse ()->setHttpResponseCode ( 200 );
			} else{
				throw new Core_Exception('Unable to delete Book');
			}
		} catch ( Core_Exception $ce ) {
			$this->getResponse ()->setBody('Core Exception Error !\n' . $ce->getMessage());
			$this->getResponse ()->setHttpResponseCode ( 500 );
		} catch ( Zend_Exception $ze ) {
			$this->getResponse ()->setBody('Zend Exception Error !\n'.$ze->getMessage());
			$this->getResponse ()->setHttpResponseCode ( 500 );
		}
		
	}

	private function log($message= '', $priority = Zend_Log::ERR) {
		$log = Zend_Registry::get('Zend_Log');
		if( $log instanceof Zend_Log) {
			$log->log($message, $priority);
		}
	}
}
