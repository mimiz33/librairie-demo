<?php 
/**
 * @license_Application_file_docblock@
 */
/**
 * Demos_StandardController
 *
 * Standard Controller for Demos Module
 *
 * @category   Application
 * @package    Demos
 * @subpackage Controller
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class StandardController extends Core_Controller_Demos_Action
{
	public function _init() {
		
	}
	
} 