<?php
/**
@license_Application_file_docblock@
 * 
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 */
/**
 * IndexController
 *
 * Default Index Controller
 *
 * @category   Application
 * @package    Default
 * @subpackage Controller
 * @copyright  @@copyright@@
 * @license    @@license@@
 * @version    @@version@@
 *
 */
class IndexController extends Zend_Controller_Action
{

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
    	
    }

    /**
     * Visualize the home page of the application !
     */
    public function indexAction()
    {
    	$this->view->tools = Core_Tools::getAllImplementations();
    }

}

