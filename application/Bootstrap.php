<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

	/**
	 * Zend auto loader initialisation
	 */
	protected function _initAutoloader() {
		$autoloader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));
		return $autoloader;
	}

	/**
	 * Initialisation des Routes
	 */
	protected function _initRoute() {
		$this->bootstrap ('frontController');
		$frontController = Zend_Controller_Front::getInstance();
		$restRoute = new Zend_Rest_Route($frontController, array(), array('api' => array('rest')));
		$frontController->getRouter ()->addRoute ('rest', $restRoute);

		$frontController->getRouter()->addRoute('demos', 
					new Zend_Controller_Router_Route('/demos/tool/:name/*', 
						array(
							'module' => 'demos',
							'controller' => 'index',
							'action' => 'demos')));

	}

	// 	protected function _initPlugins(){
	// 		$this->bootstrap ( 'frontController' );
	// 		$frontController = Zend_Controller_Front::getInstance ();

	// 		$protect = new Core_Controller_Plugin_CsrfProtect(array(
	// 				/*
	// 				'expiryTime' => 60, //will make the CSRF key expire in 60 seconds. Defaults to 5 minutes
	// 				'keyName' => 'safetycheck' //will make the CSRF form element be called "safetycheck". Defaults to "csrf"
	// 				*/
	// 		));
	// 		$frontController->registerPlugin($protect);
	// 	}

	protected function _initNavigation() {
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		$view->addHelperPath('Core/View/Helper', 'Core_View_Helper');

		$view->registerHelper(new Core_View_Helper_Navigation_Bootstrapmenu(), 'bootstrapmenu');
		$config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/navigation.xml', 'nav');
		$navigation = new Zend_Navigation($config);
		$view->navigation($navigation);
	}

	protected function _initLog() {
		$logger = new Zend_Log();

		$writerError = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../logs/errors.log');
		$logger->addWriter($writerError);

		Zend_Registry::set("Zend_Log", $logger);

	}

	protected function _initBaseStyle() {
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');

		$view = $layout->getView();
		$view->headTitle("Librairie Demo For JS Library");
		$view->headLink()->appendStylesheet('/style/bootstrap/css/bootstrap.min.css');
		$view->headLink()->appendStylesheet('/tools/google-code-prettify/prettify.css');
		//$view->headLink()->appendStylesheet('/tools/google-code-prettify/sunburst.css');
		$view->headLink()->appendStylesheet('/style/index/style.css');

		$view->headScript()->appendFile('/tools/jquery-1.7.2.min.js');
		$view->headScript()->appendFile('/tools/google-code-prettify/prettify.js');
		$view->headScript()->appendFile('/style/bootstrap/js/bootstrap.min.js');
		$view->headScript()->appendFile('/script.js');

	}

	/**
	 * Initialisation de la langue
	 */
	protected function _initLanguage() {
		if (!Zend_Registry::isRegistered("LANGUAGE")) {
			Zend_Registry::set("LANGUAGE", 'fr');
		}
		$lang = Zend_Registry::get("LANGUAGE");
		$locale = new Zend_Locale($lang);
		Zend_Registry::set('Zend_Locale', $locale);

		$translationFile = APPLICATION_PATH.DIRECTORY_SEPARATOR.'languages'.DIRECTORY_SEPARATOR.$lang.'.inc.php';

		$translate = new Zend_Translate('array', $translationFile, $lang);
		$translate->setOptions(array(
			'log' => Zend_Registry::get('Zend_Log'),
			'logUntranslated' => true
		));
		Zend_Registry::set('Zend_Translate', $translate);
	}

	protected function _initImplementations() {
		$someservice = $this->getOption('implementations');
		Zend_Registry::set('IMPLEMENTATION', $someservice);
	}

}

