CREATE TABLE librairie (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title VARCHAR(64) NOT NULL,
    author VARCHAR(64) NOT NULL,
    image VARCHAR(256) NOT NULL,
    isbn VARCHAR(32) NOT NULL,
    synopsis TEXT NULL,
    category VARCHAR(64) NOT NULL
);
 
CREATE INDEX "id" ON "librairie" ("id");