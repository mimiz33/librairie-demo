<?php 
require 'Core/Tools/File.php';

class Core_Tools_FileTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * @expectedException Core_Tools_Exception
	 */
	public function testLoadFromFileShouldThrowExceptionWhenNull() {
		Core_Tools_File::loadFromFile(null);
	}
	
	/**
	 * @expectedException Core_Tools_Exception
	 */
	public function testLoadFromFileShouldThrowExceptionWhenEmpty() {
		Core_Tools_File::loadFromFile();
	}
	
	/**
	 * @expectedException Core_Tools_Exception
	 */
	public function testLoadFromFileShouldThrowExceptionWhenNotAFile() {
		Core_Tools_File::loadFromFile("file");
	}
	
	/**
	 * 
	 */
	public function testLoadFromFileShouldReturnTool() {
		$this->assertInstanceOf("Core_Tools_File", Core_Tools_File::loadFromFile(APPLICATION_PATH . '/../tests/datas/infos.json'));
	}
	
	/**
	 *
	 */
	public function testLoadFromFileShouldReturnToolWhenRelativeFile() {
		$this->assertInstanceOf("Core_Tools_File", Core_Tools_File::loadFromFile('jquery/infos.json'));
	}
	
	/**
	 * @expectedException Core_Tools_Exception
	 */
	public function testLoadFromJsonShouldThrowExceptionWhenNull() {
		Core_Tools_File::loadFromJson(null);
	}
	
	/**
	 * @expectedException Core_Tools_Exception
	 */
	public function testLoadFromJsonShouldThrowExceptionWhenEmpty() {
		Core_Tools_File::loadFromJson();
	}
	
	/**
	 * @expectedException Core_Tools_Exception
	 */
	public function testLoadFromJsonShouldThrowExceptionWhenBadJson() {
		Core_Tools_File::loadFromJson('blahblah');
	}
}