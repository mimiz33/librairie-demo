<?php
require 'Core/Form.php';

class Core_FormTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * @dataProvider formTypes
	 */
	public function testFactoryShouldReturnFormObjects($type, $class) {
		 $this->assertInstanceOf('Core_Form',Core_Form::factory($type));
		 $this->assertInstanceOf($class,Core_Form::factory($type));
		 
	}
	
	/**
	 * @expectedException Core_Form_Exception
	 */
	public function testFactoryShouldThrowsExceptionOnUnknownType() {
		Core_Form::factory('blahblah');
	}
	
	
	/**
	 * different form types 
	 */
	public function formTypes() {
		return array(
			array('book', 'Application_Model_Forms_Book'),
			array('author', 'Application_Model_Forms_Author'),
			array('genre', 'Application_Model_Forms_Genre'));
	}
}