<?php 
require 'Core/Service/Model.php';

class Core_Service_ModelTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * @dataProvider dataTypes
	 */
	public function testFactoryShouldReturnZendDbTable($type, $class) {
		 $this->assertInstanceOf('Zend_Db_Table_Abstract',Core_Service_Model::factory($type));
		 $this->assertInstanceOf($class,Core_Service_Model::factory($type));
		 
	}
	
	/**
	 * @expectedException Core_Service_Exception
	 */
	public function testFactoryShouldThrowsExceptionOnUnknownType() {
		Core_Service_Model::factory('blahblah');
	}
	
	
	/**
	 * different table name
	 */
	public function dataTypes() {
		return array(
			array('book', 'Application_Model_DbTable_Book'),
			array('author', 'Application_Model_DbTable_Author'),
			array('genre', 'Application_Model_DbTable_Genre'));
	}
}