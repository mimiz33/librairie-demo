<?php

class StandardControllerTest extends Zend_Test_PHPUnit_ControllerTestCase {

	public function setUp() {
		$this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH.'/configs/application.ini');
		parent::setUp();
	}

	public function testAPPENV() {
		$this->assertEquals('testing', APPLICATION_ENV);
	}

	/**
	 * Index Action should be forwarded to ListAction
	 */
	public function testStandardIndexShouldForwardToListAction() {
		$this->dispatch('/standard');
		$this->assertAction('list');
		$this->assertController('standard');
		$this->assertModule('default');
		$this->assertResponseCode(200);
	}

	/**
	 * Test if Standard Action (Index Or other) should show Menu element
	 */
	public function testListActionShouldShowActionMenu() {
		$this->dispatch('/standard');
		$this->assertQueryCount('#actions-menu', 1);
	}

	/**
	 * @dataProvider getMenuItems
	 */
	public function testStandardActionsShouldShowAllMenu($id, $contains, $message) {
		$this->dispatch('/standard');
		$this->assertQueryCount('a#'.$id.'[href="'.$contains.'"]', 1, $message);

	}

	/**
	 * Test That index Action as Menu Item : List books Selected
	 */
	public function testListBookMenuItemShouldHaveSelectedClass() {
		$this->dispatch('/standard');
		$this->assertQueryCount('a#list-book.selected', 1, "List Book Menu item is not selected");
	}

	/**
	 * Test every Actions has a menu Element and that the menu item is selected
	 * @dataProvider getMenuItems
	 */
	public function testMenuSelectionsItem($id, $url, $message) {
		$this->dispatch($url);
		// Test Menu element present
		$this->assertQueryCount('#actions-menu', 1);
		// Test that each page select the good menu item
		$this->assertQueryCount('a#'.$id.'.selected', 1, $message." and selected");
	}

	/**
	 * Test every forms has an id and a reset (cancel) button
	 * @dataProvider getAllForms
	 */
	public function testFormsIdAndElements($url, $id) {
		$this->dispatch($url);
		// Test that form are present with the good id
		$this->assertQueryCount('form#'.$id, 1);

		// Test that form are present with the a cancel button
		$this->assertQueryCount('form#'.$id.' input[type="submit"]', 1);
	}

	// 	TODO Ne semble pas testable !
	// 	public function testThatFormShouldUseFactory() {
	// 		$class = $this->getMockClass(
	//           'Core_Form',          /* name of class to mock     */
	//           array('factory') /* list of methods to mock   */
	//         );

	//         $class::staticExpects($this->once())
	//               ->method('factory')
	//               ->will($this->returnValue(new Core_Form()));
	//         $this->dispatch('/standard/add/what/author');

	// 	}

	/**
	 * Add Forms should not validate With no Datas
	 * @dataProvider getAddForms
	 */
	public function testAddFormsShouldNoBeValidatedWithNoDatas($url, $id) {
		$this->request->setMethod('POST');
		$this->dispatch($url);
		$this->assertQueryCountMin('form#'.$id.' ul.errors', 1);
	}

	/**
	 * Add Genre should not work with no CSRF HASH
	 */
	public function testAddGenreCSRFProtection() {
		$this->request->setMethod('POST')->setPost(array('label' => 'Test'));
		$this->dispatch('/standard/add/what/genre');
		$this->assertQueryCountMin('form#form-add-genre ul.errors', 1);
	}

	
	/**
	 * What parameter should be defined for list actions
	 */
	public function testListActionShouldThrowExceptionWhenNoWhat() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/list');
		$this->assertController('error');
	}
	
	/**
	 * What parameter should be defined for add actions
	 */
	public function testAddActionShouldThrowExceptionWhenNoWhat() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/add');
		$this->assertController('error');
	}
	
	/**
	 * What parameter should be defined for edit actions
	 */
	public function testEditActionShouldThrowExceptionWhenNoWhat() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/edit');
		$this->assertController('error');
	}
	
	/**
	 * What parameter should be defined for delete actions
	 */
	public function testDeleteActionShouldThrowExceptionWhenNoWhat() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/delete');
		$this->assertController('error');
	}
	
	/** ADDING DATAS**/
	
	/**
	 * Test Adding A Genre
	 */
	public function testAddActionForGenre() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/add/what/genre');
		$this->assertController('standard');
		$this->assertAction('add');
		
		require_once 'Zend/Session/Namespace.php';
		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
		$this->resetRequest()->resetResponse();
		
		$this->request->setMethod('POST')->setPost(array('csrf'=>$session->hash,'id' => -1, 'label'=>'GenreTest'.time()));
		$this->dispatch('/standard/add/what/genre');
		$this->assertRedirect();
	}
	
	/**
	 * Test Adding An Author
	 */
	public function testAddActionForAuthor() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/add/what/author');
		$this->assertController('standard');
		$this->assertAction('add');
	
		require_once 'Zend/Session/Namespace.php';
		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
		$this->resetRequest()->resetResponse();
	
		$this->request->setMethod('POST')->setPost(array(
				'csrf'=>$session->hash,
				'id' => -1, 
				'name'=>'name'.time(),
				'surname'=>'surname'.time(),
				));
		$this->dispatch('/standard/add/what/author');
		$this->assertRedirect();
	}
	
	/**
	 * Test Adding A Book
	 */
	public function testAddActionForBook() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/add/what/book');
		$this->assertController('standard');
		$this->assertAction('add');
	
		require_once 'Zend/Session/Namespace.php';
		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
		$this->resetRequest()->resetResponse();
	
		$this->request->setMethod('POST')->setPost(array(
				'csrf'=>$session->hash,
				'id' => -1,
				'title'=>'title'.time(),
				'description'=>'description'.time(),
				'author' => 1,
				'genre_id' => 1
		));
		$this->dispatch('/standard/add/what/book');
		$this->assertRedirect();
	}
	
	/** EDITING DATAS **/
	
	/**
	 * Test Adding A Genre
	 */
	public function testEditActionForGenre() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/edit/what/genre/id/1');
		$this->assertController('standard');
		$this->assertAction('edit');
	
		require_once 'Zend/Session/Namespace.php';
		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
		$this->resetRequest()->resetResponse();
	
		$this->request->setMethod('POST')->setPost(array('csrf'=>$session->hash,'id' => 1, 'label'=>'Javascript'));
		$this->dispatch('/standard/edit/what/genre/id/1');
		$this->assertRedirect();
	}
	
	/**
	 * Test Adding An Author
	 */
	public function testEditActionForAuthor() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/edit/what/author/id/1');
		$this->assertController('standard');
		$this->assertAction('edit');
	
		require_once 'Zend/Session/Namespace.php';
		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
		$this->resetRequest()->resetResponse();
	
		$this->request->setMethod('POST')->setPost(array(
				'csrf'=>$session->hash,
				'id' => 1,
				'name'=>'Resig',
				'surname'=>'John',
		));
		$this->dispatch('/standard/edit/what/author/id/1');
		$this->assertRedirect();
	}
	
	/**
	 * Test Adding A Book
	 */
	public function testEditActionForBook() {
		$this->request->setMethod('GET');
		$this->dispatch('/standard/edit/what/book/id/1');
		$this->assertController('standard');
		$this->assertAction('edit');
	
		require_once 'Zend/Session/Namespace.php';
		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
		$this->resetRequest()->resetResponse();
	
		$this->request->setMethod('POST')->setPost(array(
				'csrf'=>$session->hash,
				'id' => 1,
				'title'=>'Javascript : The Good Parts',
				'description'=>'Most programming languages contain good and bad parts, but JavaScript has more than its share of the bad, having been developed and released in a hurry before it could be refined. This authoritative book scrapes away these bad features to reveal a subset of JavaScript that\'s more reliable, readable, and maintainable than the language as a whole-a subset you can use to create truly extensible and efficient code.',
				'author' => 2,
				'genre_id' => 1
		));
		$this->dispatch('/standard/edit/what/book/id/1');
		$this->assertRedirect();
	}
	
	/** DELETE Actions **/
	
	/**
	 * Test Deleting An Author
	 */
// 	public function testDeleteActionForAuthor() {
// 		$this->request->setMethod('GET');
// 		$this->dispatch('/standard/delete/what/author/id/8');
// 		$this->assertController('standard');
// 		$this->assertAction('delete');
	
// 		require_once 'Zend/Session/Namespace.php';
// 		$session = new Zend_Session_Namespace('Zend_Form_Element_Hash_salt_csrf');
// 		$this->resetRequest()->resetResponse();
	
// 		$this->request->setMethod('POST')->setPost(array(
// 				'csrf'=>$session->hash,
// 				'id' => 8,
// 				'what'=>'author'
// 		));
// 		$this->dispatch('/standard/delete/what/author/id/8');
// 		$this->assertRedirect();
// 	}
	
	
	
	/** DATAS PROVIDERS **/

	/**
	 * return all Menu items that should be present in menu
	 * dataprovider for testMenuSelectionsItem / testStandardActionsShouldShowAllMenu
	 */
	public function getMenuItems() {
		return array(
			array('add-book', '/standard/add/what/book', 'Add Book item should be present'),
			array('add-author', '/standard/add/what/author', 'Add Author item should be present'),
			array('add-genre', '/standard/add/what/genre', 'Add Genre item should be present'),
			array('list-book', '/standard/list/what/book', 'List Book item should be present'),
			array('list-author', '/standard/list/what/author', 'List Author item should be present'),
			array('list-genre', '/standard/list/what/genre', 'List Genre item should be present'),
		);
	}

	/**
	 * Return all form (url, id)
	 * @return multitype:
	 */
	public function getAllForms() {
		return array(
			array('/standard/add/what/book', 'form-add-book', 'Application_Model_Forms_Book'),
			array('/standard/add/what/author', 'form-add-author', 'Application_Model_Forms_Author'),
			array('/standard/add/what/genre', 'form-add-genre', 'Application_Model_Forms_Genre'),
			array('/standard/edit/what/book/id/1', 'form-edit-book', 'Application_Model_Forms_Book'),
			array('/standard/edit/what/author/id/1', 'form-edit-author', 'Application_Model_Forms_Author'),
			array('/standard/edit/what/genre/id/1', 'form-edit-genre', 'Application_Model_Forms_Genre'),
			array('/standard/delete/what/book/id/1', 'form-delete-book'),
			array('/standard/delete/what/author/id/1', 'form-delete-author'),
			array('/standard/delete/what/genre/id/1', 'form-delete-genre')
		);
	}

	public function getAddForms() {
		return array(
			array('/standard/add/what/book', 'form-add-book'),
			array('/standard/add/what/author', 'form-add-author'),
			array('/standard/add/what/genre', 'form-add-genre')
		);
	}

}

