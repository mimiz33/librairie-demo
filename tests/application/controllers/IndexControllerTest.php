<?php

class IndexControllerTest extends Zend_Test_PHPUnit_ControllerTestCase
{

    public function setUp()
    {
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        parent::setUp();
    }

    
    public function testAPPENV() {
    	$this->assertEquals('testing', APPLICATION_ENV);
    }
    
    /**
     * Test la page d'accueil
     */
    public function testIndexAction() {
    	$this->dispatch('/');
    	$this->assertAction('index');
    	$this->assertController('index');
    	$this->assertModule('default');
    	$this->assertResponseCode(200);
    	$this->assertQueryCount('#content.container',1);
    }
    
    

    
}

